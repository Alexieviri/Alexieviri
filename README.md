<!-- ### Hi there 👋 -->
<!--
![Header](https://github.com/Alexieviri/Alexieviri/blob/main/assets/header.gif?raw=true)
-->
![Header](https://github.com/Alexieviri/Alexieviri/blob/main/assets/header_v3.gif?raw=true)
### About me

Hi there 👋 My name is Alexander. I work as Data Engineer for BMW project and study Data Science at SSAU. 

Research work at the university is related to the preprocessing of social network users' data and its detailed analysis in order to generate hypotheses for further research. The current research is aimed at building a social scoring model using machine learning methods.

You can find more information about me on my LinkedIn page.
### Languages and Tools
![Python](https://img.shields.io/badge/-Python-000000?style=flat&logo=Python&logoColor=yellow&color=white)
![R](https://img.shields.io/badge/-R-000000?style=flat&logo=R&logoColor=blue&color=white)
![SQL](https://img.shields.io/badge/-SQL-000000?style=flat&logo=SQLite&logoColor=pink&color=white)
![AWS](https://img.shields.io/badge/-Amazon_AWS-000000?style=flat&logo=amazon-aws&logoColor=orange&color=white)
![Hadoop](https://img.shields.io/badge/-Hadoop-000000?style=flat&logo=ApacheHadoop&logoColor=yellow&color=white)
![Spark](https://img.shields.io/badge/-Spark-000000?style=flat&logo=Apachespark&logoColor=red&color=white)
![Kafka](https://img.shields.io/badge/-Kafka-000000?style=flat&logo=ApacheKafka&logoColor=red&color=white)
![Docker](https://img.shields.io/badge/-Docker-000000?style=flat&logo=Docker&logoColor=blue&color=white)
![Terraform](https://img.shields.io/badge/-Terraform-000000?style=flat&logo=Terraform&logoColor=yellow&color=white)
![Hive](https://img.shields.io/badge/-Hive-000000?style=flat&logo=Hive&logoColor=purple&color=white)
![Snowflake](https://img.shields.io/badge/-Snowflake-000000?style=flat&logo=Snowflake&logoColor=blue&color=white)

### Contact me
[![LinkedIn](https://img.shields.io/badge/-LinkedIN-000000?style=flat&logo=LinkedIn&logoColor=blue)](https://www.linkedin.com/in/alexstadnikov/)
[![WhatsUp](https://img.shields.io/badge/WhatsApp-000000?style=flat&logo=whatsapp&logoColor=25D366)](https://wa.me/79992277252?text=%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82!%20%F0%9F%91%8B%20)
[![Telegram](https://img.shields.io/badge/Telegram-000000?style=flat&logo=telegram&logoColor=white)](https://t.me/Alexieviri)
[![GMAIL](https://img.shields.io/badge/Gmail-000000?style=flat&logo=gmail&logoColor=D14836)](st054825@student.spbu.ru)

<!-- ![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=alexieviri&hide=issues,prs&show_icons=true&theme=radical)
-->
<!--
**Alexieviri/Alexieviri** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
